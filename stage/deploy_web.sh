# go to root directory
cd ~

# stop forever

# del existing old directory
rm -rf b2b_web_old

# mv current repo to old
mv ~/b2b_web ~/b2b_web_old

# clone new repo
git clone git@bitbucket.org:ozandlb/b2b_web.git

# go to directory
cd b2b_web

# checkout correct branch
git checkout -b develop origin/develop

# change api server host
sed -i 's/127.0.0.1/stage.jamb2b.com/g' dist/scripts/*.scripts.js

# back to ~/
cd ~

# move ssl certificates to app
cp -R b2b_sslcert/stage/ b2b_web/

# change name of ssl directory
mv b2b_web/stage b2b_web/sslcert

# restart forever

