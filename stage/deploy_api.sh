# go to root directory
cd ~

# del existing old directory
rm -rf b2b_server_old

# mv current repo to old
mv ~/b2b_server ~/b2b_server_old

# clone new repo
git clone git@bitbucket.org:ozandlb/b2b_server.git

# go to directory
cd b2b_server

# checkout correct branch
git checkout -b develop origin/develop

# remove node_modules
rm -rf node_modules

# fresh install node modules
npm install

# back to ~/
cd ~

# move ssl certificates to app
cp -R b2b_sslcert/stage/ b2b_server/

# change name of ssl directory
mv b2b_server/stage b2b_server/sslcert

# move dbconfigs into project directory
cp b2b_dbconfigs/stage.json b2b_server/config/config.json


